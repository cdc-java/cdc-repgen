package cdc.repgen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Description of an index.
 * <p>
 * It is composed of:
 * <ul>
 * <li>A <b>title</b>.
 * <li>A <b>menu</b>.
 * <li>A <b>subtitle</b>.
 * <li>A list of <b>index entries</b>.
 * </ul>
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class RGIndex<E> {
    private final RGName title;
    private final RGMenu menu;
    private final RGName subtitle;
    private final List<RGIndexEntry<E>> entries = new ArrayList<>();

    protected RGIndex(RGName title,
                      RGMenu menu,
                      RGName subtitle,
                      List<RGIndexEntry<E>> entries) {
        this.title = title;
        this.menu = menu;
        this.subtitle = subtitle;
        this.entries.addAll(entries);
    }

    public RGName getTitle() {
        return title;
    }

    public RGMenu getMenu() {
        return menu;
    }

    public RGName getSubtitle() {
        return subtitle;
    }

    public List<RGIndexEntry<E>> getEntries() {
        return entries;
    }

    public static <E> Builder<E> builder(Class<E> cls) {
        return new Builder<>();
    }

    public static class Builder<E> {
        protected RGName title;
        protected RGMenu menu;
        private RGName subtitle;
        private final List<RGIndexEntry<E>> entries = new ArrayList<>();

        protected Builder() {
            super();
        }

        public Builder<E> title(RGName title) {
            this.title = title;
            return this;
        }

        public Builder<E> menu(RGMenu menu) {
            this.menu = menu;
            return this;
        }

        public Builder<E> subtitle(RGName subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public Builder<E> entry(RGIndexEntry<E> entry) {
            this.entries.add(entry);
            return this;
        }

        public Builder<E> entries(List<RGIndexEntry<E>> entries) {
            this.entries.addAll(entries);
            return this;
        }

        public RGIndex<E> build() {
            return new RGIndex<>(title,
                                 menu,
                                 subtitle,
                                 entries);
        }
    }
}