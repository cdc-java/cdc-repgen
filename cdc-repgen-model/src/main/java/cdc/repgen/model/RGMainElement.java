package cdc.repgen.model;

import java.util.List;

/**
 * Description of a main frame associated to an element.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class RGMainElement<E> extends RGMain {
    private final E element;

    public RGMainElement(RGName title,
                         RGMenu menu,
                         RGName subtitle,
                         List<RGSection> sections,
                         E element) {
        super(title, menu, subtitle, sections);
        this.element = element;
    }

    public E getElement() {
        return element;
    }
}