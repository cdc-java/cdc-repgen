package cdc.repgen.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RGTableSection extends RGSection {
    private final RGTableRow header;
    private final List<RGTableRow> data;
    private final String tableClass;
    private final List<String> dataClasses;

    protected RGTableSection(String title,
                             RGTableRow header,
                             List<RGTableRow> data,
                             String tableClass,
                             List<String> dataClasses) {
        super(title);
        this.header = header;
        this.data = data;
        this.tableClass = tableClass;
        this.dataClasses = dataClasses;
    }

    public RGTableRow getHeader() {
        return header;
    }

    public List<RGTableRow> getData() {
        return data;
    }

    public String getTableClass() {
        return tableClass;
    }

    public List<String> getDataClasses() {
        return dataClasses;
    }

    public String getDataClass(int index) {
        if (index >= 0 && index < dataClasses.size()) {
            return dataClasses.get(index);
        } else {
            return null;
        }
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends RGSection.Builder<B> {
        protected RGTableRow header;
        protected final List<RGTableRow> data = new ArrayList<>();
        protected String tableClass;
        protected final List<String> dataClasses = new ArrayList<>();

        protected Builder() {
            super();
        }

        @Override
        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public B header(RGTableRow header) {
            this.header = header;
            return self();
        }

        public B data(RGTableRow data) {
            this.data.add(data);
            return self();
        }

        public B data(List<RGTableRow> data) {
            this.data.addAll(data);
            return self();
        }

        public B tableClass(String tableClass) {
            this.tableClass = tableClass;
            return self();
        }

        public B dataClass(String dataClass) {
            this.dataClasses.add(dataClass);
            return self();
        }

        public B dataClasses(List<String> dataClasses) {
            this.dataClasses.addAll(dataClasses);
            return self();
        }

        public B dataClasses(String... dataClasses) {
            Collections.addAll(this.dataClasses, dataClasses);
            return self();
        }

        @Override
        public RGTableSection build() {
            return new RGTableSection(title,
                                      header,
                                      data,
                                      tableClass,
                                      dataClasses);
        }
    }
}