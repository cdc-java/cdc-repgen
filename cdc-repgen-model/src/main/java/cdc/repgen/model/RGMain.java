package cdc.repgen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class of a main frames.
 * <p>
 * It is composed of:
 * <ul>
 * <li>A <b>title</b>.
 * <li>A <b>menu</b>.
 * <li>A <b>subtitle</b>.
 * <li>A list of <b>sections</b>.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class RGMain {
    private final RGName title;
    private final RGMenu menu;
    private final RGName subtitle;
    private final List<RGSection> sections = new ArrayList<>();

    public RGMain(RGName title,
                  RGMenu menu,
                  RGName subtitle,
                  List<RGSection> sections) {
        this.title = title;
        this.menu = menu;
        this.subtitle = subtitle;
        this.sections.addAll(sections);
    }

    public RGName getTitle() {
        return title;
    }

    public RGMenu getMenu() {
        return menu;
    }

    public RGName getSubtitle() {
        return subtitle;
    }

    public List<RGSection> getSections() {
        return sections;
    }
}