package cdc.repgen.model;

/**
 * Description of an index entry.
 * <p>
 * It is {@link RGName} with an associated element.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class RGIndexEntry<E> extends RGName {
    /** The associated element. */
    private final E element;

    protected RGIndexEntry(String text,
                           String href,
                           String target,
                           String icon,
                           E element) {
        super(text,
              href,
              target,
              icon);
        this.element = element;
    }

    public E getElement() {
        return element;
    }

    public static <E> Builder<E, ?> builder(E element) {
        return new Builder<>(element);
    }

    public static class Builder<E, B extends Builder<E, B>> extends RGName.Builder<B> {
        protected final E element;

        protected Builder(E element) {
            super();
            this.element = element;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected B self() {
            return (B) this;
        }

        @Override
        public RGIndexEntry<E> build() {
            return new RGIndexEntry<>(text,
                                      href,
                                      target,
                                      imgSrc,
                                      element);
        }
    }
}