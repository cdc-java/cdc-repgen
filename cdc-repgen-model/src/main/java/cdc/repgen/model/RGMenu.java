package cdc.repgen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A Menu is a list of {@link RGMenuItem}s.
 *
 * @author Damien Carbonne
 */
public class RGMenu {
    private final List<RGMenuItem> items = new ArrayList<>();

    public RGMenu(List<RGMenuItem> items) {
        this.items.addAll(items);
    }

    public List<RGMenuItem> getItems() {
        return items;
    }
}