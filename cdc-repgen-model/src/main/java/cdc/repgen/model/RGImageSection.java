package cdc.repgen.model;

/**
 * Section containing an image.
 *
 * @author Damien Carbonne
 */
public class RGImageSection extends RGSection {
    private final String name;

    protected RGImageSection(String title,
                             String name) {
        super(title);
        this.name = cdc.util.files.Files.toValidBasename(name);
    }

    public String getName() {
        return name;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends RGSection.Builder<B> {
        protected String name;

        protected Builder() {
            super();
        }

        @Override
        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public B name(String name) {
            this.name = name;
            return self();
        }

        @Override
        public RGImageSection build() {
            return new RGImageSection(title, name);
        }
    }
}