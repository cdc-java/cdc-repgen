package cdc.repgen.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RGTableRow {
    private final List<RGName> cells;

    public RGTableRow(List<RGName> cells) {
        this.cells = new ArrayList<>(cells);
    }

    public RGTableRow(RGName... cells) {
        this.cells = new ArrayList<>();
        Collections.addAll(this.cells, cells);
    }

    public RGTableRow(String... cells) {
        this.cells = new ArrayList<>();
        for (final String cell : cells) {
            this.cells.add(RGName.builder().text(cell).build());
        }
    }

    public List<RGName> getCells() {
        return cells;
    }
}