package cdc.repgen.model;

/**
 * Base class of sections.
 * <p>
 * Each section has a <b>title</b>.
 *
 * @author Damien Carbonne
 */
public class RGSection {
    protected final String title;

    protected RGSection(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> {
        protected String title;

        protected Builder() {
            super();
        }

        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public B title(String title) {
            this.title = title;
            return self();
        }

        public RGSection build() {
            return new RGSection(title);
        }
    }
}